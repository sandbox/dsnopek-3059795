/**
 * @file
 * Javascript for accordion region.
 */

(function ($) {

Drupal.behaviors.block_style_plugins_ng_example_accordion = {
  attach: function (context, settings) {
    $('.block-style-plugins-ng-examples-region-accordion', context).accordion();
  }
};

})(jQuery);