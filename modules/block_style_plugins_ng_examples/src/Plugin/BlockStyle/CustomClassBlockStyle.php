<?php

namespace Drupal\block_style_plugins_ng_examples\Plugin\BlockStyle;

use Drupal\block_style_plugins_ng\Plugin\Annotation\BlockStyle;
use Drupal\block_style_plugins_ng\Plugin\BlockStyleDefault;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Class ComplexBlockStyle
 *
 * @BlockStyle(
 *   id = "block_style_plugin_ng_examples_custom_class",
 *   label = @Translation("Custom CSS class(es)"),
 *   library = "block_style_plugins_ng_examples/examples",
 * )
 */
class CustomClassBlockStyle extends BlockStyleDefault implements PluginFormInterface {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'css_class_names' => [],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getConfiguration();

    $form['css_class_names'] = [
      '#type' => 'textfield',
      '#title' => t('Custom CSS class(es)'),
      '#required' => TRUE,
      '#default_value' => isset($settings['css_class_names']) && is_array($settings['css_class_names']) ? implode(' ', $settings['css_class_names']) : '',
      '#description' => t("Please enter any arbitrary CSS class names seperated by a space.<br /><strong>Hint: Try <em>block-style-plugins-ng-examples-custom</em> - it'll make the widget bright red!</strong>"),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $css_class_names = $this->parseCssClassNames($form_state->getValue('css_class_names'));
    if (in_array('invalid', $css_class_names)) {
      $form_state->setError($form['css_class_names'], t("Can't use CSS class name 'invalid'"));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->getConfiguration();
    $settings['css_class_names'] = $this->parseCssClassNames($form_state->getValue('css_class_names'));
    $this->setConfiguration($settings);
  }

  /**
   * {@inheritDoc}
   */
  public function renderBlock(array $build, PluginInspectionInterface $plugin, $in_preview = FALSE) {
    $build = parent::renderBlock($build, $plugin, $in_preview);

    $configuration = $this->getConfiguration();

    if (isset($configuration['css_class_names']) && is_array($configuration['css_class_names'])) {
      foreach ($configuration['css_class_names'] as $class_name) {
        $build['#attributes']['class'][] = $class_name;
      }
    }

    return $build;
  }

  /**
   * Parse the CSS class names from a string.
   *
   * This separate the class names into an array and trim whitespace.
   *
   * @return string[]
   *   The CSS class names.
   */
  protected function parseCssClassNames($string = NULL) {
    return array_filter(array_map('trim', explode(' ', $string)));
  }

}