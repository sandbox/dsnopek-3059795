<?php

namespace Drupal\block_style_plugins_ng\Alterations;

use Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;

class ElementInfoAlter implements TrustedCallbackInterface {

  /**
   * The region style manager.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager
   */
  protected $regionStyleManager;

  /**
   * ElementInfoAlter constructor.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager $region_style_manager
   *   The region style manager.
   */
  public function __construct(RegionStylePluginManager $region_style_manager) {
    $this->regionStyleManager = $region_style_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['layoutBuilderPreRender'];
  }

  public function alterInfo(&$info) {
    // Skip this if Layout Builder is not installed, or if the core patch that
    // allows for section styles is not applied.
    // @see https://www.drupal.org/project/drupal/issues/3062862
    if (isset($info['layout_builder']) && class_exists('Drupal\layout_builder\Event\SectionBuildRegionsRenderArrayEvent')) {
      $info['layout_builder']['#pre_render'][] = 'block_style_plugins_ng.alterations.element_info_alter:layoutBuilderPreRender';
    }
  }

  public function layoutBuilderPreRender($element) {
    $region_style_definitions = $this->regionStyleManager->getDefinitions();

    if (count($region_style_definitions) > 0) {
      // If there are any region styles, then we want to make sure that all
      // regions with any blocks have the "Configure style" link.
      foreach (Element::children($element['layout_builder']) as $name) {
        if (is_array($element['layout_builder'][$name]) && !empty($element['layout_builder'][$name]['layout-builder__section'])) {
          $build =& $element['layout_builder'][$name]['layout-builder__section'];
          foreach (Element::children($build) as $region) {
            $route_parameters = NULL;
            foreach (Element::children($build[$region]) as $block_uuid) {
              if (!empty($build[$region][$block_uuid]['#contextual_links']['layout_builder_block'])) {
                $route_parameters = $build[$region][$block_uuid]['#contextual_links']['layout_builder_block']['route_parameters'];
                break;
              }
            }

            // This means there are no blocks in this region.
            if ($route_parameters === NULL) {
              continue;
            }

            $build[$region]['block_style_plugins_ng_configure_style'] = [
              '#type' => 'container',
              '#weight' => -1000,
              '#attributes' => [
                'class' => [
                  'layout-builder__block-style-plugins-ng__configure-region-style',
                ],
              ]
            ];
            $build[$region]['block_style_plugins_ng_configure_style']['link'] = [
              '#type' => 'link',
              // Add one to the current delta since it is zero-indexed.
              // @todo Add hidden text for accessibility
              '#title' => t('Configure region style'),
              '#url' => Url::fromRoute('block_style_plugins_ng.layout_builder_configure_region_style',
                [
                  'section_storage_type' => $route_parameters['section_storage_type'],
                  'section_storage' => $route_parameters['section_storage'],
                  'delta' => $route_parameters['delta'],
                  'region' => $route_parameters['region'],
                ],
                [
                  'attributes' => [
                    'class' => [
                      'use-ajax',
                      'layout-builder__link',
                      'layout-builder__link--configure',
                    ],
                    'data-dialog-type' => 'dialog',
                    'data-dialog-renderer' => 'off_canvas',
                  ],
                ]
              ),
            ];
          }
        }
      }

      $element['#attached']['library'][] = 'block_style_plugins_ng/layout_builder';
    }

    return $element;
  }

}