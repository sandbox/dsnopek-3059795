<?php


namespace Drupal\block_style_plugins_ng\Alterations;


use Drupal\block_style_plugins_ng\Plugin\BlockStyleDefinition;
use Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * A service for altering the block form to add style configuration.
 */
class BlockFormAlter {

  /**
   * @var \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager
   */
  protected $blockStyleManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * BlockFormAlter constructor.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager $block_style_manager
   *   The block style plugin manager.
   */
  public function __construct(BlockStylePluginManager $block_style_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->blockStyleManager = $block_style_manager;
    $this->loggerFactory = $logger_factory;
  }

  public function alterForm(array &$form, FormStateInterface $form_state, $form_id) {
    /** @var \Drupal\block\Entity\Block $entity */
    $entity = $form_state->getFormObject()->getEntity();

    $style_plugin_options = ['' => t('- None -')];
    $style_plugin_options += $this->blockStyleManager->getBlockStyleOptions($entity->getPluginId());

    if ($form_state->hasValue(['third_party_settings', 'block_style_plugins_ng'])) {
      $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    }
    else {
      $style_plugin_settings = $entity->getThirdPartySettings('block_style_plugins_ng');
    }

    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;
    $style_plugin = $this->getPlugin($style_plugin_settings);

    $form['third_party_settings']['block_style_plugins_ng'] = [
      '#type' => 'fieldset',
      '#title' => t('Block style'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => [
        'id' => Html::getUniqueId('block-style-plugin-settings'),
      ],
    ];
    $form['third_party_settings']['block_style_plugins_ng']['id'] = [
      '#type' => 'select',
      '#title' => t('Style'),
      '#options' => $style_plugin_options,
      '#default_value' => $style_plugin_id,
      '#ajax' => [
        'callback' => [static::class, 'ajaxCallback'],
        'event' => 'change',
        'wrapper' => $form['third_party_settings']['block_style_plugins_ng']['#attributes']['id'],
      ],
    ];

    if ($style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'], $form, $form_state);
      $form['third_party_settings']['block_style_plugins_ng'] = $style_plugin->buildConfigurationForm($form['third_party_settings']['block_style_plugins_ng'], $subform_state);

      $form['actions']['submit']['#validate'][] = [$this, 'validatePluginForm'];
      array_unshift($form['actions']['submit']['#submit'], [$this, 'submitPluginForm']);

      // Prevent form caching because otherwise the above callbacks will cause
      // errors when attempting to serialize $this into the cache.
      $form_state->disableCache();
    }
  }

  /**
   * Load the plugin from settings.
   *
   * @param array $style_plugin_settings
   *   The style plugin settings.
   *
   * @return \Drupal\block_style_plugins_ng\Plugin\BlockStyleInterface|NULL
   *   The block style object or NULL if it can't be initialized.
   */
  protected function getPlugin(array $style_plugin_settings) {
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    $style_plugin = NULL;
    if ($style_plugin_id) {
      try {
        $style_plugin = $this->blockStyleManager->createInstance($style_plugin_id, $style_plugin_settings);
      }
      catch (PluginException $e) {
        $this->loggerFactory->get('block_style_plugins_ng')->error('Unable to instantiate style plugin {style_plugin_id}: {message}', [
          'style_plugin_id' => $style_plugin_id,
          'message' => $e->getMessage(),
          'exception' => $e,
        ]);

        $style_plugin = NULL;
        $style_plugin_id = NULL;
      }
    }

    return $style_plugin;
  }

  public static function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['third_party_settings']['block_style_plugins_ng'];
  }

  public function validatePluginForm(array $form, FormStateInterface $form_state) {
    $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    $style_plugin = $this->getPlugin($style_plugin_settings);
    if ($style_plugin && $style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'], $form, $form_state);
      $style_plugin->validateConfigurationForm($form['third_party_settings']['block_style_plugins_ng'], $subform_state);
    }
  }

  public function submitPluginForm(array $form, FormStateInterface $form_state) {
    $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    $style_plugin = $this->getPlugin($style_plugin_settings);

    if ($style_plugin && $style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'], $form, $form_state);
      $style_plugin->submitConfigurationForm($form['third_party_settings']['block_style_plugins_ng'], $subform_state);
      $form_state->setValue(['third_party_settings', 'block_style_plugins_ng'], $style_plugin->getConfiguration());
    }
  }

}