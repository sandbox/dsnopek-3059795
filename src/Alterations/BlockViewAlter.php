<?php


namespace Drupal\block_style_plugins_ng\Alterations;


use Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class BlockViewAlter {

  /**
   * @var \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager
   */
  protected $blockStyleManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * BlockFormAlter constructor.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager $block_style_manager
   *   The block style plugin manager.
   */
  public function __construct(BlockStylePluginManager $block_style_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->blockStyleManager = $block_style_manager;
    $this->loggerFactory = $logger_factory;
  }

  public function alterView(&$build, $block_plugin) {
    if (!isset($build['#block'])) {
      return;
    }

    /** @var \Drupal\block\Entity\Block $entity */
    $entity = $build['#block'];
    $style_plugin_settings = $entity->getThirdPartySettings('block_style_plugins_ng');
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    if ($style_plugin_id) {
      try {
        $style_plugin = $this->blockStyleManager->createInstance($style_plugin_id, $style_plugin_settings);
      } catch (PluginException $e) {
        $this->loggerFactory->get('block_style_plugins_ng')->error('Unable to instantiate style plugin {style_plugin_id}: {message}', [
          'style_plugin_id' => $style_plugin_id,
          'message' => $e->getMessage(),
          'exception' => $e,
        ]);

        return;
      }

      $build = $style_plugin->renderBlock($build, $entity->getPlugin());
    }
  }

}