<?php

namespace Drupal\block_style_plugins_ng\EventSubscriber;

use Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\SectionComponent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds Layout Builder component styles.
 */
class SectionComponentStyles implements EventSubscriberInterface {

  /**
   * The block styles manager.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager
   */
  protected $blockStyleManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs SectionComponentStyles.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager $block_style_manager
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct(BlockStylePluginManager $block_style_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->blockStyleManager = $block_style_manager;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    // Skip this if the Layout Builder is not installed.
    if (class_exists('\Drupal\layout_builder\LayoutBuilderEvents')) {
      $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = 'onBuildRender';
    }

    return $events;
  }

  /**
   * Add styles to a section component.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component build render array event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $style_plugin_settings = $this->getStylePluginSettings($event->getComponent());
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    if ($style_plugin_id) {
      try {
        $style_plugin = $this->blockStyleManager->createInstance($style_plugin_id, $style_plugin_settings);
      }
      catch (PluginException $e) {
        $this->loggerFactory->get('block_style_plugins_ng')
          ->error('Unable to instantiate style plugin {style_plugin_id}: {message}', [
            'style_plugin_id' => $style_plugin_id,
            'message' => $e->getMessage(),
            'exception' => $e,
          ]);
        return;
      }

      $build = $event->getBuild();
      $build = $style_plugin->renderBlock($build, $event->getPlugin(), $event->inPreview());
      $event->setBuild($build);
    }
  }

  protected function getStylePluginSettings(SectionComponent $component) {
    // @todo Remove once components support third-party settings.
    // @see https://www.drupal.org/project/drupal/issues/3015152
    if (method_exists($component, 'getThirdPartySettings')) {
      $style_plugin_settings = $component->getThirdPartySettings('block_style_plugins_ng');
    }
    else {
      $style_plugin_settings = $component->get('block_style_plugins_ng');
      if ($style_plugin_settings === NULL) {
        $style_plugin_settings = [];
      }
    }
    return $style_plugin_settings;
  }


}
