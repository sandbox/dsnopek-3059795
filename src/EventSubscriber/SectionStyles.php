<?php

namespace Drupal\block_style_plugins_ng\EventSubscriber;

use Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\layout_builder\Event\SectionBuildRegionsRenderArrayEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds Layout Builder section styles.
 */
class SectionStyles implements EventSubscriberInterface {

  /**
   * The region styles manager.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager
   */
  protected $regionStyleManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs SectionStyles.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager $region_style_manager
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct(RegionStylePluginManager $region_style_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->regionStyleManager = $region_style_manager;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    // Skip this if Layout Builder is not installed, or if the core patch that
    // allows for section styles is not applied.
    // @see https://www.drupal.org/project/drupal/issues/3062862
    if (class_exists('Drupal\layout_builder\Event\SectionBuildRegionsRenderArrayEvent')) {
      $events[SectionBuildRegionsRenderArrayEvent::class] = ['onBuildRegionsRenderArray', 1000];
    }

    return $events;
  }

  /**
   * Add styles to section regions.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component build render array event.
   */
  public function onBuildRegionsRenderArray(SectionBuildRegionsRenderArrayEvent $event) {
    // Prevent layout_builders normal region building.
    $event->stopPropagation();

    $regions = $event->getRegions();
    $section = $event->getSection();
    $layout = $section->getLayout();

    $components = [];
    foreach ($event->getSection()->getComponents() as $component) {
      $components[$component->getRegion()][$component->getUuid()] = $component;
    }

    foreach ($layout->getPluginDefinition()->getRegionNames() as $region) {
      if (empty($components[$region])) {
        continue;
      }

      // Render with the configured region style.
      $region_style_settings = $section->getThirdPartySetting('block_style_plugins_ng', $region);
      $region_style = $this->getPluginFromSettings($region_style_settings);
      if ($region_style) {
        $regions[$region] = $region_style->renderRegion($components[$region], $layout, $region, $event->getContexts(), $event->isInPreview());
      }
      else {
        // Or if no region style is configured, simply render each component.
        foreach ($components[$region] as $key => $component) {
          $regions[$region][$key] = $component->toRenderArray($event->getContexts(), $event->isInPreview());
        }
      }
    }

    $event->setRegions($regions);
  }

  protected function getPluginFromSettings($region_style_settings) {
    $region_style = NULL;

    if (!empty($region_style_settings) && !empty($region_style_settings['id'])) {
      $region_style_id = $region_style_settings['id'];
      try {
        $region_style = $this->regionStyleManager->createInstance($region_style_id, $region_style_settings);
      }
      catch(PluginException $e) {
        $this->loggerFactory->get('block_style_plugins_ng')->error('Unable to instantiate style plugin {style_plugin_id}: {message}', [
          'style_plugin_id' => $region_style_id,
          'message' => $e->getMessage(),
          'exception' => $e,
        ]);
      }
    }

    return $region_style;
  }

}
