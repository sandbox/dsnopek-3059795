<?php

namespace Drupal\block_style_plugins_ng\Plugin\Annotation;

use Drupal\block_style_plugins_ng\Plugin\BlockStyleDefault;
use Drupal\block_style_plugins_ng\Plugin\BlockStyleDefinition;
use Drupal\Component\Annotation\Plugin;

/**
 * Defines a BlockStyle annotation object.
 *
 * Block styles are used to apply changes to rendered blocks.
 *
 * Plugin namespace: Plugin\BlockStyle
 *
 * @see \Drupal\block_style_plugins_ng\Plugin\BlockStyleInterface
 * @see \Drupal\block_style_plugins_ng\Plugin\BlockStyleDefault
 * @see \Drupal\block_style_plugins_ng\Plugin\BlockStyleDefinition
 * @see \Drupal\block_style_plugins_ng\Plugin\BlockStyleManager
 * @see plugin_api
 *
 * @Annotation
 */
class BlockStyle extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * An optional description for advanced styles.
   *
   * Sometimes styles are so complex that the name is insufficient to describe
   * a style such that a visually impaired administrator could style a page
   * for a non-visually impaired audience. If specified, it will provide a
   * description that is used for accessibility purposes.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The template file to render this style (relative to the 'path' given).
   *
   * If specified, then the block_style_plugins_ng module will register the
   * template with hook_theme() and the module or theme registering this style
   * does not need to do it.
   *
   * @var string optional
   *
   * @see hook_theme()
   */
  public $template;

  /**
   * The theme hook used to render this style.
   *
   * If specified, it's assumed that the module or theme registering this style
   * will also register the theme hook with hook_theme() itself. This is
   * mutually exclusive with 'template' - you can't specify both.
   *
   * @var string optional
   *
   * @see hook_theme()
   */
  public $theme_hook = 'block';

  /**
   * Path (relative to the module or theme) to the template.
   *
   * @var string optional
   */
  public $path;

  /**
   * CSS class names to add to the block.
   *
   * @var string[] optional
   */
  public $css_class_names = [];

  /**
   * The asset library.
   *
   * @var string optional
   */
  public $library;

  /**
   * Block types to exclude.
   *
   * @var string[] optional
   */
  public $exclude = [];

  /**
   * Include only these block types.
   *
   * @var string[] optional
   */
  public $include = [];

  /**
   * The block style plugin class.
   *
   * This default value is used for plugins defined in MODULE.block_style.yml
   * that do not specify a class themselves.
   *
   * @var string
   */
  public $class = BlockStyleDefault::class;

  /**
   * {@inheritdoc}
   */
  public function get() {
    return new BlockStyleDefinition($this->definition);
  }

}
