<?php

namespace Drupal\block_style_plugins_ng\Plugin;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Provides an interface for BlockStyle plugins.
 */
interface BlockStyleInterface extends PluginInspectionInterface, DerivativeInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Build a render array for block with the style applied.
   *
   * @param array $block
   *   The render array of a built block.
   * @param array $plugin
   *   The block plugin that was rendered.
   * @param bool $in_preview
   *   TRUE if we're in the preview; FALSE otherwise.
   *
   * @return array
   *   Render array for the block with the style applied.
   */
  public function renderBlock(array $block, PluginInspectionInterface $plugin, $in_preview = FALSE);

}
