<?php

namespace Drupal\block_style_plugins_ng\Plugin;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Layout\LayoutInterface;

/**
 * Provides an interface for RegionStyle plugins.
 */
interface RegionStyleInterface extends PluginInspectionInterface, DerivativeInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Build a render array for region with the style applied.
   *
   * @param array $blocks_or_components
   *   The blocks or components to render.
   * @param \Drupal\Core\Layout\LayoutInterface $layout
   *   The layout.
   * @param string $region_name
   *   The machine name of the region.
   * @param array|\Drupal\Core\Plugin\Context\ContextInterface[] $contexts
   *   The contexts.
   * @param bool $in_preview
   *   TRUE if we're in the preview; FALSE otherwise.
   *
   * @return array
   *   Render array for the region with the style applied.
   */
  public function renderRegion(array $blocks_or_components, LayoutInterface $layout, $region_name, array $contexts = [], $in_preview = FALSE);

}
