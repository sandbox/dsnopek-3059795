<?php

namespace Drupal\block_style_plugins_ng\Plugin;

use Drupal\block\BlockInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Layout\LayoutInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\layout_builder\SectionComponent;

/**
 * Provides a default class for RegionStyle plugins.
 */
class RegionStyleDefault extends PluginBase implements RegionStyleInterface {

  /**
   * The region style definition.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\RegionStyleDefinition
   */
  protected $pluginDefinition;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function renderRegion(array $blocks_or_components, LayoutInterface $layout, $region_name, array $contexts = [], $in_preview = FALSE) {
    $labels = [];
    $build = [];
    foreach ($blocks_or_components as $key => $block_or_component) {
      $labels[$key] = $this->getLabelForBlockOrComponent($block_or_component, $contexts);
      if (!$in_preview && $this->pluginDefinition->getHideBlockLabels()) {
        $this->hideLabelForBlockOrComponent($block_or_component, $contexts);
      }

      if ($rendered_block_or_component = $this->renderBlockOrComponent($block_or_component, $contexts, $in_preview)) {
        $build[$key] = $rendered_block_or_component;
      }
    }

    if (!$in_preview) {
      $build['#theme'] = $this->pluginDefinition->getThemeHook();
      $build['#region'] = $region_name;
      $build['#labels'] = $labels;

      foreach ($this->pluginDefinition->getCssClassNames() as $class_name) {
        $build['#attributes']['class'][] = $class_name;
      }

      if ($library = $this->pluginDefinition->getLibrary()) {
        $build['#attached']['library'][] = $library;
      }
    }

    return $build;
  }

  /**
   * Renders the block or component.
   *
   * @param BlockPluginInterface|SectionComponent $block_or_component
   *   The block or component.
   * @param array|\Drupal\Core\Plugin\Context\ContextInterface[] $contexts
   *   The contexts.
   * @param bool $in_preview
   *   Whether or not this is the preview.
   *
   * @return array|NULL
   */
  protected function renderBlockOrComponent($block_or_component, array $contexts, $in_preview) {
    if ($block_or_component instanceof BlockPluginInterface) {
      return [
        '#theme' => 'block',
        '#configuration' => $block_or_component->getConfiguration(),
        '#plugin_id' => $block_or_component->getPluginId(),
        '#base_plugin_id' => $block_or_component->getBaseId(),
        '#derivative_plugin_id' => $block_or_component->getDerivativeId(),
        'content' => $block_or_component->build(),
      ];
    }
    if ($block_or_component instanceof SectionComponent) {
      return $block_or_component->toRenderArray($contexts, $in_preview);
    }

    return NULL;
  }

  /**
   * Gets the label from the block or component.
   *
   * @param BlockPluginInterface|SectionComponent $block_or_component
   *   The block or component.
   * @param array|\Drupal\Core\Plugin\Context\ContextInterface[] $contexts
   *   The contexts.
   *
   * @return string
   */
  protected function getLabelForBlockOrComponent($block_or_component, array $contexts) {
    if ($block_or_component instanceof SectionComponent) {
      $block_or_component = $block_or_component->getPlugin($contexts);
    }
    if ($block_or_component instanceof BlockPluginInterface) {
      return $block_or_component->label();
    }
    return '';
  }

  /**
   * Hides the label on the block or component.
   *
   * @param BlockPluginInterface|SectionComponent $block_or_component
   *   The block or component.
   * @param array|\Drupal\Core\Plugin\Context\ContextInterface[] $contexts
   *   The contexts
   */
  protected function hideLabelForBlockOrComponent($block_or_component, array $contexts) {
    $component = NULL;
    if ($block_or_component instanceof SectionComponent) {
      $component = $block_or_component;
      $block_or_component = $block_or_component->getPlugin($contexts);
    }
    if ($block_or_component instanceof BlockPluginInterface) {
      $block_or_component->setConfigurationValue('label_display', '0');
      if ($component) {
        $component->setConfiguration($block_or_component->getConfiguration());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
