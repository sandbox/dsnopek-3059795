<?php

namespace Drupal\block_style_plugins_ng\Plugin;

use Drupal\block_style_plugins_ng\Plugin\Annotation\RegionStyle;
use Drupal\Component\Annotation\Plugin\Discovery\AnnotationBridgeDecorator;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\Core\Plugin\FilteredPluginManagerInterface;
use Drupal\Core\Plugin\FilteredPluginManagerTrait;

/**
 * Provides a plugin manager for region styles.
 */
class RegionStylePluginManager extends DefaultPluginManager implements FilteredPluginManagerInterface {

  use FilteredPluginManagerTrait;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * RegionStylePluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler, EntityRepositoryInterface $entity_repository) {
    parent::__construct('Plugin/RegionStyle', $namespaces, $module_handler, RegionStyleInterface::class, RegionStyle::class);
    $this->themeHandler = $theme_handler;
    $this->entityRepository = $entity_repository;

    $this->alterInfo('block_style_plugins_ng_region_styles');
    $this->setCacheBackend($cache_backend, 'block_style_plugins_ng_region_styles');
  }

  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return 'region_styles';
  }

  /**
   * {@inheritdoc}
   */
  protected function providerExists($provider) {
    return $this->moduleHandler->moduleExists($provider) || $this->themeHandler->themeExists($provider);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $discovery = new YamlDiscoveryDecorator($discovery, $this->getType(), $this->moduleHandler->getModuleDirectories() + $this->themeHandler->getThemeDirectories());
      $discovery
        ->addTranslatableProperty('label')
        ->addTranslatableProperty('description');
      $discovery = new AnnotationBridgeDecorator($discovery, $this->pluginDefinitionAnnotationName);
      $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
      $this->discovery = $discovery;
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    if (!$definition instanceof RegionStyleDefinition) {
      throw new InvalidPluginDefinitionException($plugin_id, sprintf('The "%s" region style definition must extend %s', $plugin_id, RegionStyleDefinition::class));
    }

    // Add the module or theme path to the 'path'.
    $provider = $definition->getProvider();
    if ($this->moduleHandler->moduleExists($provider)) {
      $base_path = $this->moduleHandler->getModule($provider)->getPath();
    }
    elseif ($this->themeHandler->themeExists($provider)) {
      $base_path = $this->themeHandler->getTheme($provider)->getPath();
    }
    else {
      $base_path = '';
    }

    $path = $definition->getPath();
    $path = !empty($path) ? $base_path . '/' . $path : $base_path;
    $definition->setPath($path);

    // Add a dependency on the provider of the library.
    if ($library = $definition->getLibrary()) {
      $config_dependencies = $definition->getConfigDependencies();
      list($library_provider) = explode('/', $library, 2);
      if ($this->moduleHandler->moduleExists($library_provider)) {
        $config_dependencies['module'][] = $library_provider;
      }
      elseif ($this->themeHandler->themeExists($library_provider)) {
        $config_dependencies['theme'][] = $library_provider;
      }
      $definition->setConfigDependencies($config_dependencies);
    }

    // If 'template' is set, then we'll derive 'template_path' and 'theme_hook'.
    $template = $definition->getTemplate();
    if (!empty($template)) {
      $template_parts = explode('/', $template);

      $template = array_pop($template_parts);
      $template_path = $path;
      if (count($template_parts) > 0) {
        $template_path .= '/' . implode('/', $template_parts);
      }
      $definition->setTemplate($template);
      $definition->setThemeHook(strtr($template, '-', '_'));
      $definition->setTemplatePath($template_path);
    }
  }

  public function getThemeImplementations() {
    $hooks = [];

    $hooks['block_style_plugins_ng_region'] = [
      'render element' => 'content',
      'template' => 'block-style-plugins-ng-region',
    ];

    /** @var \Drupal\block_style_plugins_ng\Plugin\RegionStyleDefinition[] $definitions */
    $definitions = $this->getDefinitions();
    foreach ($definitions as $definition) {
      if ($template = $definition->getTemplate()) {
        $hooks[$definition->getThemeHook()] = [
          'render element' => 'content',
          'base hook' => 'block_style_plugins_ng_region',
          'template' => $template,
          'path' => $definition->getTemplatePath(),
        ];
      }
    }
    return $hooks;
  }

  /**
   * Gets region style options.
   *
   * @return string[]
   *   An associative array mapping plugin ids to plugin labels.
   */
  public function getRegionStyleOptions() {
    $options = [];
    /**
     * @var string $style_id
     * @var \Drupal\block_style_plugins_ng\Plugin\RegionStyleDefinition $style_plugin
     */
    foreach ($this->getDefinitions() as $style_id => $style_plugin) {
      $options[$style_id] = $style_plugin->getLabel();
    }

    return $options;
  }

}
