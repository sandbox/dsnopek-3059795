<?php

namespace Drupal\block_style_plugins_ng\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a default class for BlockStyle plugins.
 */
class BlockStyleDefault extends PluginBase implements BlockStyleInterface {

  /**
   * The block style definition.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\BlockStyleDefinition
   */
  protected $pluginDefinition;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function renderBlock(array $build, PluginInspectionInterface $plugin, $in_preview = FALSE) {
    $build['#theme'] = $this->pluginDefinition->getThemeHook();

    foreach ($this->pluginDefinition->getCssClassNames() as $class_name) {
      $build['#attributes']['class'][] = $class_name;
    }

    if ($library = $this->pluginDefinition->getLibrary()) {
      $build['#attached']['library'][] = $library;
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
