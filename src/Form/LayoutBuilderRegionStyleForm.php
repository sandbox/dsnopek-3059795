<?php

namespace Drupal\block_style_plugins_ng\Form;

use Drupal\block_style_plugins_ng\Plugin\BlockStyleDefinition;
use Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager;
use Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for applying styles to a block.
 *
 * @internal
 */
class LayoutBuilderRegionStyleForm extends FormBase {

  use AjaxFormHelperTrait;
  use LayoutRebuildTrait;

  /**
   * The Block Styles Manager.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager
   */
  protected $regionStyleManager;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The section storage.
   *
   * @var \Drupal\layout_builder\SectionStorageInterface
   */
  protected $sectionStorage;

  /**
   * The layout section delta.
   *
   * @var int
   */
  protected $delta;

  /**
   * The uuid of the block component.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The machine name of the region.
   *
   * @var string
   */
  protected $region;

  /**
   * Constructs a LayoutBuilderRegionStyleForm.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\RegionStylePluginManager $region_style_manager
   *   The block style manager.
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout builder tempstore repository.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(RegionStylePluginManager $region_style_manager, LayoutTempstoreRepositoryInterface $layout_tempstore_repository, LoggerChannelFactoryInterface $logger_factory) {
    $this->regionStyleManager = $region_style_manager;
    $this->layoutTempstoreRepository = $layout_tempstore_repository;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block_style_plugins_ng_region_styles'),
      $container->get('layout_builder.tempstore_repository'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_style_plugins_ng_layout_builder_region_style_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $uuid = NULL, $region = NULL) {
    $this->sectionStorage = $section_storage;
    $this->delta = $delta;
    $this->uuid = $uuid;
    $this->region = $region;

    $section = $this->getSection();

    $style_plugin_options = ['' => t('- None -')];
    $style_plugin_options += $this->regionStyleManager->getRegionStyleOptions();

    $style_plugin_settings = $this->getStylePluginSettings($section, $form_state);
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    $style_plugin = $this->getPlugin($style_plugin_settings);

    $form['#tree'] = TRUE;

    $form['third_party_settings']['block_style_plugins_ng'][$this->region] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => Html::getUniqueId('block-style-plugin-settings'),
      ],
    ];
    $form['third_party_settings']['block_style_plugins_ng'][$this->region]['id'] = [
      '#type' => 'select',
      '#title' => t('Style'),
      '#options' => $style_plugin_options,
      '#default_value' => $style_plugin_id,
      '#ajax' => [
        'callback' => [static::class, 'ajaxStyleSettingsCallback'],
        'event' => 'change',
        'wrapper' => $form['third_party_settings']['block_style_plugins_ng'][$this->region]['#attributes']['id'],
      ],
    ];

    if ($style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'][$this->region], $form, $form_state);
      $form['third_party_settings']['block_style_plugins_ng'] = $style_plugin->buildConfigurationForm($form['third_party_settings']['block_style_plugins_ng'][$this->region], $subform_state);
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax']['callback'] = '::ajaxSubmit';
      $form['actions']['submit']['#ajax']['event'] = 'click';
    }
    return $form;
  }

  public static function ajaxStyleSettingsCallback(array &$form, FormStateInterface $form_state) {
    return $form['third_party_settings']['block_style_plugins_ng'];
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng', $this->region]);
    $style_plugin = $this->getPlugin($style_plugin_settings);

    if ($style_plugin && $style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'][$this->region], $form, $form_state);
      $style_plugin->validateConfigurationForm($form['third_party_settings']['block_style_plugins_ng'][$this->region], $subform_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng', $this->region]);
    $style_plugin = $this->getPlugin($style_plugin_settings);

    $configuration = [];
    if ($style_plugin) {
      if ($style_plugin instanceof PluginFormInterface) {
        $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'][$this->region], $form, $form_state);
        $style_plugin->submitConfigurationForm($form['third_party_settings']['block_style_plugins_ng'][$this->region], $subform_state);
      }
      $configuration = $style_plugin->getConfiguration();
    }

    $section = $this->getSection();
    $section->setThirdPartySetting('block_style_plugins_ng', $this->region, $configuration);

    $this->layoutTempstoreRepository->set($this->sectionStorage);
    $form_state->setRedirectUrl($this->sectionStorage->getLayoutBuilderUrl());
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    return $this->rebuildAndClose($this->sectionStorage);
  }

  /**
   * @return \Drupal\layout_builder\Section
   */
  protected function getSection() {
    return $this->sectionStorage->getSection($this->delta);
  }

  protected function getStylePluginSettings(Section $section, FormStateInterface $form_state) {
    if ($form_state->hasValue(['third_party_settings', 'block_style_plugins_ng'])) {
      $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    }
    else {
      $style_plugin_settings = $section->getThirdPartySettings('block_style_plugins_ng');
    }

    return !empty($style_plugin_settings[$this->region]) ? $style_plugin_settings[$this->region] : [];
  }

  protected function getPlugin($style_plugin_settings) {
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    $style_plugin = NULL;
    if ($style_plugin_id) {
      try {
        $style_plugin = $this->regionStyleManager->createInstance($style_plugin_id, $style_plugin_settings);
      }
      catch (PluginException $e) {
        $this->loggerFactory->get('block_style_plugins_ng')->error('Unable to instantiate style plugin {style_plugin_id}: {message}', [
          'style_plugin_id' => $style_plugin_id,
          'message' => $e->getMessage(),
          'exception' => $e,
        ]);

        $style_plugin = NULL;
        $style_plugin_id = NULL;
      }
    }

    return $style_plugin;
  }

}
