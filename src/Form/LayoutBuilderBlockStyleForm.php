<?php

namespace Drupal\block_style_plugins_ng\Form;

use Drupal\block_style_plugins_ng\Plugin\BlockStyleDefinition;
use Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for applying styles to a block.
 *
 * @internal
 */
class LayoutBuilderBlockStyleForm extends FormBase {

  use AjaxFormHelperTrait;
  use LayoutRebuildTrait;

  /**
   * The Block Styles Manager.
   *
   * @var \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager
   */
  protected $blockStyleManager;

  /**
   * The layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The section storage.
   *
   * @var \Drupal\layout_builder\SectionStorageInterface
   */
  protected $sectionStorage;

  /**
   * The layout section delta.
   *
   * @var int
   */
  protected $delta;

  /**
   * The uuid of the block component.
   *
   * @var string
   */
  protected $uuid;

  /**
   * Constructs a BlockStylesForm object.
   *
   * @param \Drupal\block_style_plugins_ng\Plugin\BlockStylePluginManager $blockStyleManager
   *   The block style manager.
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layoutTempstoreRepository
   *   The layout builder tempstore repository.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(BlockStylePluginManager $blockStyleManager, LayoutTempstoreRepositoryInterface $layoutTempstoreRepository, LoggerChannelFactoryInterface $loggerFactory) {
    $this->blockStyleManager = $blockStyleManager;
    $this->layoutTempstoreRepository = $layoutTempstoreRepository;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block_style_plugins_ng'),
      $container->get('layout_builder.tempstore_repository'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_style_plugins_ng_layout_builder_block_style_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $uuid = NULL) {
    $this->sectionStorage = $section_storage;
    $this->delta = $delta;
    $this->uuid = $uuid;

    $component = $this->getComponent();

    $style_plugin_options = ['' => t('- None -')];
    $style_plugin_options += $this->blockStyleManager->getBlockStyleOptions($component->getPluginId());

    $style_plugin_settings = $this->getStylePluginSettings($component, $form_state);
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    $style_plugin = $this->getPlugin($style_plugin_settings);

    $form['#tree'] = TRUE;

    $form['third_party_settings']['block_style_plugins_ng'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => Html::getUniqueId('block-style-plugin-settings'),
      ],
    ];
    $form['third_party_settings']['block_style_plugins_ng']['id'] = [
      '#type' => 'select',
      '#title' => t('Style'),
      '#options' => $style_plugin_options,
      '#default_value' => $style_plugin_id,
      '#ajax' => [
        'callback' => [static::class, 'ajaxStyleSettingsCallback'],
        'event' => 'change',
        'wrapper' => $form['third_party_settings']['block_style_plugins_ng']['#attributes']['id'],
      ],
    ];

    if ($style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'], $form, $form_state);
      $form['third_party_settings']['block_style_plugins_ng'] = $style_plugin->buildConfigurationForm($form['third_party_settings']['block_style_plugins_ng'], $subform_state);
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax']['callback'] = '::ajaxSubmit';
      $form['actions']['submit']['#ajax']['event'] = 'click';
    }
    return $form;
  }

  public static function ajaxStyleSettingsCallback(array &$form, FormStateInterface $form_state) {
    return $form['third_party_settings']['block_style_plugins_ng'];
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    $style_plugin = $this->getPlugin($style_plugin_settings);

    if ($style_plugin && $style_plugin instanceof PluginFormInterface) {
      $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'], $form, $form_state);
      $style_plugin->validateConfigurationForm($form['third_party_settings']['block_style_plugins_ng'], $subform_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    $style_plugin = $this->getPlugin($style_plugin_settings);

    $configuration = [];
    if ($style_plugin) {
      if ($style_plugin instanceof PluginFormInterface) {
        $subform_state = SubformState::createForSubform($form['third_party_settings']['block_style_plugins_ng'], $form, $form_state);
        $style_plugin->submitConfigurationForm($form['third_party_settings']['block_style_plugins_ng'], $subform_state);
      }
      $configuration = $style_plugin->getConfiguration();
    }

    $component = $this->getComponent();
    $this->setStylePluginSettings($component, $configuration);

    $this->layoutTempstoreRepository->set($this->sectionStorage);
    $form_state->setRedirectUrl($this->sectionStorage->getLayoutBuilderUrl());
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    return $this->rebuildAndClose($this->sectionStorage);
  }

  /**
   * @return \Drupal\layout_builder\SectionComponent
   */
  protected function getComponent() {
    return $this->sectionStorage->getSection($this->delta)->getComponent($this->uuid);
  }

  protected function getStylePluginSettings(SectionComponent $component, FormStateInterface $form_state) {
    if ($form_state->hasValue(['third_party_settings', 'block_style_plugins_ng'])) {
      $style_plugin_settings = $form_state->getValue(['third_party_settings', 'block_style_plugins_ng']);
    }
    // @todo Remove once components support third-party settings.
    // @see https://www.drupal.org/project/drupal/issues/3015152
    elseif (method_exists($component, 'getThirdPartySettings')) {
      $style_plugin_settings = $component->getThirdPartySettings('block_style_plugins_ng');
    }
    else {
      $style_plugin_settings = $component->get('block_style_plugins_ng');
      if ($style_plugin_settings === NULL) {
        $style_plugin_settings = [];
      }
    }
    return $style_plugin_settings;
  }

  protected function setStylePluginSettings(SectionComponent $component, $style_plugin_settings) {
    // @todo Remove once components support third-party settings.
    // @see https://www.drupal.org/project/drupal/issues/3015152
    if (method_exists($component, 'setThirdPartySetting')) {
      foreach ($style_plugin_settings as $style_plugin_setting_name => $style_plugin_setting) {
        $component->setThirdPartySetting('block_style_plugins_ng', $style_plugin_setting_name, $style_plugin_setting);
      }
    }
    else {
      $component->set('block_style_plugins_ng', $style_plugin_settings);
    }
  }

  protected function getPlugin($style_plugin_settings) {
    $style_plugin_id = !empty($style_plugin_settings['id']) ? $style_plugin_settings['id'] : NULL;

    $style_plugin = NULL;
    if ($style_plugin_id) {
      try {
        $style_plugin = $this->blockStyleManager->createInstance($style_plugin_id, $style_plugin_settings);
      }
      catch (PluginException $e) {
        $this->loggerFactory->get('block_style_plugins_ng')->error('Unable to instantiate style plugin {style_plugin_id}: {message}', [
          'style_plugin_id' => $style_plugin_id,
          'message' => $e->getMessage(),
          'exception' => $e,
        ]);

        $style_plugin = NULL;
        $style_plugin_id = NULL;
      }
    }

    return $style_plugin;
  }

}
